package org.sas.pendulum;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.List;

public class WorldM implements PositionSource {

List<PendulumM> pendulums = new ArrayList<PendulumM>();
	private int cnt;
	
	public WorldM(){
		//http://www.arborsci.com/cool/pendulum-wave-seems-like-magic-but-its-physics 
		for ( double i = 0; i < 16;++i){
			pendulums.add(new PendulumM(600*Math.pow(18.0/(18.0+i), 2.0),20.0,Math.PI/25));
		}
	}
	
	public List<Point2D.Double> getPositions() {
		List<Point2D.Double> posList = new ArrayList<Point2D.Double>();
		for (PendulumM pendulum : pendulums) {
			posList.add(pendulum.next(.02*cnt++));
		};
		return posList;
	}


}
