package org.sas.pendulum;

import java.awt.geom.Point2D;
import java.util.List;

public interface PositionSource {
	List<Point2D.Double> getPositions();
}
