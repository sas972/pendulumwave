package org.sas.pendulum;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class World implements PositionSource{

	List<Pendulum> pendulums = new ArrayList<Pendulum>();
	
	public World(){
		//http://www.arborsci.com/cool/pendulum-wave-seems-like-magic-but-its-physics 
		for ( double i = 0; i < 16;++i){
			pendulums.add(new Pendulum(600*Math.pow(18.0/(18.0+i), 2.0),2,Math.PI/25));
		}
	}
	
	public List<Point2D.Double> getPositions() {
		List<Point2D.Double> posList = new ArrayList<Point2D.Double>();
		for (Pendulum pendulum : pendulums) {
			posList.add(pendulum.next());
		};
		return posList;
	}

}
