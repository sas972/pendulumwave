package org.sas.pendulum;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

import java.awt.geom.Point2D;

public class PendulumM {
	private Point2D.Double pos = new Point2D.Double();
	double l, g, alpha, omega, beta, alpha0;
	
	public PendulumM(double l, double g, double alpha) {
		super();
		this.l = l;
		this.g = g;
		this.alpha0 = alpha;
		omega = Math.sqrt(g/this.l);
	}
	
	public  Point2D.Double next(double t){
		alpha = alpha0*cos(omega*t);
		pos.x = l*sin(alpha);
		pos.y = l*cos(alpha);
		return pos; 	
	}
	
}
