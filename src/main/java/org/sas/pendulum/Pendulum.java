package org.sas.pendulum;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

import java.awt.geom.Point2D;

public class Pendulum {

	private Point2D.Double pos = new Point2D.Double();
	double l, g, alpha, omega, beta;
	
	public Pendulum() {
		super();
	}

	public Pendulum(double l, double g, double alpha) {
		super();
		this.l = l;
		this.g = g;
		this.alpha = alpha;
	}
	public  Point2D.Double next(){
		double a = -1* g * sin(alpha);
		beta = a/l;
		omega += beta;
		alpha += omega;
		pos.x = l*sin(alpha);
		pos.y = l*cos(alpha);
		return pos; 	
	}

	public Point2D.Double getPos() {
		return pos;
	}

	public void setPos(Point2D.Double pos) {
		this.pos = pos;
	}

	public double getL() {
		return l;
	}

	public void setL(double l) {
		this.l = l;
	}

	public double getG() {
		return g;
	}

	public void setG(double g) {
		this.g = g;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public double getOmega() {
		return omega;
	}

	public void setOmega(double omega) {
		this.omega = omega;
	}

	public double getBeta() {
		return beta;
	}

	public void setBeta(double beta) {
		this.beta = beta;
	}
	
	
}
